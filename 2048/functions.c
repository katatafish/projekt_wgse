#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>


void moveUp(int field[4][4])
{
		for(int i=0;i<4;i++){
		for(int n=3;n>=0;n--){
			int maxField = n;
			int check = 1;
			int differentOneInLine = false;
			while(check<=maxField){
				if(field[n][i]!=0){
					if(field[n-check][i]!=field[n][i] && field[n-check][i]!=0){
					differentOneInLine=true;
					}
	
				if(field[n][i]==field[n-check][i] || field[n-check][i]==0){		
				//	if(differentOneInLine==false){
						field[n-check][i]= field[n][i]+field[n-check][i]; 
						field[n][i] = 0;
			//		}
				}
			
				}
				check++;
					
			}
		}
	}
}


void moveDown(int field[4][4])
{
		for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			int maxField = 3-n;
			int check = 1;
			int differentOneInLine = false;
			while(check<=maxField){
				if(field[n][i]!=0){
					if(field[n+check][i]!=field[n][i] && field[n+check][i]!=0){
					differentOneInLine=true;
					}

				if(field[n][i]==field[n+check][i] || field[n+check][i]==0){
				//	if(differentOneInLine==false){
						field[n+check][i]= field[n][i]+field[n+check][i];
						field[n][i] = 0;
				//	}
				}

				}
				check++;

			}
		}
	}
}

void moveRight(int field[4][4])
{
		for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			int maxField = 3-n;
			int check = 1;
			int differentOneInLine = false;
			while(check<=maxField){
				if(field[i][n]!=0){
					if(field[i][n+check]!=field[i][n] && field[i][n+check]!=0){
					differentOneInLine=true;
					}

				if(field[i][n]==field[i][n+check] || field[i][n+check]==0){
				//	if(differentOneInLine==false){
						field[i][n+check]= field[i][n]+field[i][n+check];
						field[i][n] = 0;
				//	}
				}

				}
				check++;

			}
		}
	}
}

void moveLeft(int field[4][4])
{
		for(int i=0;i<4;i++){
		for(int n=3;n>=0;n--){
			int maxField = n;
			int check = 1;
			int differentOneInLine = false;
			while(check<=maxField){
				if(field[i][n]!=0){
					if(field[i][n-check]!=field[i][n] && field[i][n-check]!=0){
					differentOneInLine=true;
					}

				if(field[i][n]==field[i][n-check] || field[i][n-check]==0){
				//	if(differentOneInLine==false){
						field[i][n-check]= field[i][n]+field[i][n-check];
						field[i][n] = 0;
			//		}
				}

				}
				check++;

			}
		}
	}
}



int checkWinCondition(int field[4][4]){
 return 1;
}

void resetField(int field[4][4]){
	for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			field[i][n]=0;
		}
	}
}

void printField(int field[4][4]){
	for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			printf("| %d |  ",field[i][n]);
		}
		printf("\n\n");
	}
}

void randomField(int field[4][4]){
	int i;
	int n;
	int r;
	bool done = false;
	srand(time(NULL));
	while(done == false){
		i = rand() % 4;
		n = rand() % 4;
		if(field[i][n]==0){
			r = rand() % 20;
			if(r < 10){
				field[i][n] = 4;
			}
			else
	        	{
				field[i][n] = 2;
			}
	 	done = true;
		}

	}
}
