/*
 * =====================================================================================
 *
 *       Filename:  2048.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12.06.2017 17:32:11
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
/**
\mainpage
\file functions.c
\brief 
Holds the functions For the field of the Game 2048
\author 
Timo Schazi
*/

/**
Moves all Values in The Array to the top and adds same values 
\param int[4][4] the Array to move Up
*/
void moveUp(int[4][4]);


/**
Moves all Values in The Array to the bottom and adds same values 
\param int[4][4] the Array to move Down
*/
void moveDown(int[4][4]);

/**
Moves all Values in The Array to the Right and adds same values 
\param int[4][4] the Array to move to the right 
*/
void moveRight(int[4][4]);

/**
Moves all Values in The Array to the Left and adds same values 
\param int[4][4] the Array to move to the left
*/
void moveLeft(int[4][4]);

/**
Sets all values in the Array to Zero
\param int[4][4] the Array to set all values to 0
*/
void resetField(int[4][4]);

/**
Prints the current Field Array
\param int[4][4] the array that has to be printed
*/
void printField(int[4][4]);

/**
 Checks if Array contains the number 2048 returns 1 if true else 0
\param int[4][4] the Array to check
\return 1 if true or 0 if false
*/
int checkWinCondition(int[4][4]);

/**
Adds a random  2 or 4 to a value in the array when its zero
\param int[4][4] the Array where a random 2 or 4 should be added 
*/
void randomField(int[4][4]);
