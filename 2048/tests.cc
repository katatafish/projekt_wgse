
/**
\file tests.cc
\brief 
Tests for the Field functions
\author 
Timo Schaz
*/







#include <stdlib.h>
#include <gtest/gtest.h>
#include "functions.h"
#include <string.h>

using namespace std;

/**
 The testField
 */
int testField[4][4];

/** 
 Tests the moveUp function with the testField
 */
TEST(moveUp, testingMoveUp) { 
   moveUp(testField);
   EXPECT_EQ(testField[0][0],4);
   EXPECT_EQ(testField[1][0],0);
   EXPECT_EQ(testField[2][0],0);
   EXPECT_EQ(testField[3][0],0);
}
/** 
 Tests the moveRight function with the testField
 */
TEST(moveRight, testingMoveRight) { 
   moveRight(testField);
   EXPECT_EQ(testField[0][3],4);
   EXPECT_EQ(testField[0][0],0);
   EXPECT_EQ(testField[2][0],0);
   EXPECT_EQ(testField[3][0],0);
}
/** 
 Tests the moveDown function with the testField
 */
TEST(moveDown, testingMoveDown) { 
   moveDown(testField);
   EXPECT_EQ(testField[3][3],4);
   EXPECT_EQ(testField[0][3],0);
   EXPECT_EQ(testField[2][0],0);
   EXPECT_EQ(testField[3][0],0);
}
/**
 Tests the moveLeft function with the testField
 */
TEST(moveLeft, testingMoveLeft) { 
   moveLeft(testField);
   EXPECT_EQ(testField[3][0],4);
   EXPECT_EQ(testField[3][3],0);
   EXPECT_EQ(testField[2][0],0);
}
/**
 Tests the resetField function with the testField
 */
TEST(resetField, testingResetField) { 
   resetField(testField);
 int  check= 0;
   for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			if(testField[i][n]!=0){
				check++;
			}
		}		
	} 

 EXPECT_EQ(check,0);
}
/**
 Tests the randomField function with the testField
 */
TEST(randomField, testingRandomField) { 
   randomField(testField);
   int check = 0;
   for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			if(testField[i][n]!=0 && (testField[i][n]== 2 ||testField[i][n]== 4  ))
			{
			 check++;	
			}
		}
	}

   EXPECT_EQ(check,1);
}

TEST(checkWinCondition, testingCheckWinCodition) { 
      	testField[1][1]=2048;
	EXPECT_EQ(checkWinCondition(testField),1);
}


int main(int argc, char **argv) {

    for(int i=0;i<4;i++){
		for(int n=0;n<4;n++){
			testField[i][n]=0;
		}
	}

    testField[2][0]=2;
    testField[3][0]=2;	

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
